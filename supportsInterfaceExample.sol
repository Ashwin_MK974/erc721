// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./interface.sol";
contract Test is SimpleInterfaceA {
        uint256 public number = 256;
        string  public phrase = "hello";

        function getString() external view returns(string memory)
        {
            return phrase;
        }

        function getNumber() external view returns(uint256)
        {
            return number;
        }

        function supportInterface(bytes4 interfaceId) external pure returns(bool)
        {
            bytes4 interfaceAId= type(SimpleInterfaceA).interfaceId;
            return interfaceId == interfaceAId; 
        }

        function getInterfaceIdFull() public pure returns(bytes4)
        {
            bytes4 interfaceAId= type(SimpleInterfaceA).interfaceId;
            return interfaceAId;
        }
/*
//keccak256("getNumber()")=>f2c9ecd8ec9cc7cb8e7819015497279c0a95e47a657075f0f863f20e4918963c
//keccak256("getString()")=>89ea642f517dcb35f0d700efe29d741904d47d1a0aa4e441db479fc1e9f95fae


A | B | A XOR B
--+---+--------
0 | 0 |   0
0 | 1 |   1
1 | 0 |   1
1 | 1 |   0
getNumber(): f2c9ecd8ec9cc7cb8e7819015497279c0a95e47a657075f0f863f20e4918963c => keccak256("getNumber)
getString(): 89ea642f517dcb35f0d700efe29d741904d47d1a0aa4e441db479fc1e9f95fae => keccak256("getString)
En bytes 4 :
getNumber(): f2c9ecd8
getString(): 89ea642f
En binaire:
getNumber(): 11110010110010011110110011011000
getString(): 10001001111010100110010000101111
Avec l'opération xor :
    11110010110010011110110011011000
XOR
    10001001111010100110010000101111
----------------------------------------
    01111011001000111000100011110111 soit en HEX : 7b2388f7
Résultat Finale de l'identifiant de l'interafce : 7b2388f7
*/
}
