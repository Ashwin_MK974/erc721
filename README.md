# Interface ERC721

Ce projet est une implémentation complète des fonctions requises pour un jeton non-fongible ERC721. En respectant les spécifications de l'interface ERC721, il garantit que les jetons sont conformes au standard et peuvent interagir avec d'autres contrats et services compatibles avec ERC721.

Dans ce document, chaque fonction essentielle définie par l'interface IERC721 sera examinée en détail, en présentant son rôle, son fonctionnement technique et en illustrant sa mise en œuvre. Ces implémentations fourniront une compréhension plus approfondie des mécanismes à mettre en place pour d'autres projets futurs.

Les fonctions examinées comprendront la création et l'émission de nouveaux jetons, la gestion de la propriété et des autorisations, les transferts et les approbations, ainsi que les mécanismes d'interaction avec d'autres contrats et services compatibles ERC721. Le projet n'utilise pas de framework ou de bibliothèque spécifique, ce qui permettra par la suite la mise en place de mécanismes pour des projets beaucoup plus complexes.

## Evenement

#### Transfer

```http
 event Transfer(address indexed from, address indexed to, uint256 _tokenId)
```
Cette fonction émet un évenement de type Transfer. Cela intervient lorsqu'un jeton à été minté ou alors transferer d'un proprietaire à un autre.
#### Approval

```
 event Approval(address indexed _owner, address indexed _approved, uint256 indexed _tokenId);
```
Cette fonction émet un évenement de type Approval. C'est lorsqu'un proprietaire, approuve quelqu'un a gérer un jeton spécifique. L'autorité est donc donné à une tierce personne, mais le proprietaire garde la totale autorité sur ce jeton spécifique.
#### ApprovalForAll
```
   event ApprovalForAll(address indexed _owner, address indexed _operator, bool _approved);//ok
```
Elle intervient dans le cadre ou un proprietaire à apporuvé à un opérateur l'accès à tout ces jetons.

#### ownerOf
```
  function ownerOf(uint256 _tokenId) view public returns(address)
```
Permet de retourner l'addresse de proprietaire d'un jeton

#### balanceOf
```
  balanceOf(address _owner)
```
Permet de retourner le nombre de token qu'un proprietaire dispose sur une collection

#### mint
```
  mint(address _to, string, memory _tokenURI)
```
Permet de créer le token et de l'affecter à un proprietaire.

#### Transfer
```
  transferer(address _from,address _to,uint256 _tokenId)
```
Permet de transférer un token d'un proprietaire à un autre, ou de quelqu'un approuvé à transférer le token vers un autre proprietaire.

#### safeTransferFrom
```
  transferer(address _from,address _to,uint256 _tokenId)
```
Permet de transférer un token d'un proprietaire à un autre, ou de quelqu'un approuvé à transférer le token vers un autre proprietaire.
